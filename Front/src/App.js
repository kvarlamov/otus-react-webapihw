import logo from './logo.svg';
import './App.css';
import {Weather} from './Weather/Weather';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />        
      </header>  
      <Weather />    
    </div>
  );
}

export default App;
