import {useEffect, useState} from "react";
import {Table} from "react-bootstrap";
import axios from "axios";

export const Weather = () => {

    const [loading, setLoading] = useState(true);
    const [forecasts, setForecasts] = useState();
    
    useEffect(() => {
        async function load(){
            const response = await axios.get('http://localhost:5055/WeatherForecast');
            setForecasts(response.data);
            setLoading(false)
        }
        load()
        
    }, [])

    let content

    function renderWeather() {
        let id = 0;
        return forecasts.map(w => {
            return (
                <tr key={id++}>
                    <td>{w.date}</td>
                    <td>{w.temperatureC}</td>
                    <td>{w.temperatureF}</td>
                    <td>{w.summary}</td>
                </tr>
            )
        })
    }

    if (loading) 
        content = <p>Loading</p>
    else
        content = <Table striped bordered hover style={{width: "100%"}}>
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>temperatureC</th>
                        <th>temperatureF</th>
                        <th>summary</th>
                    </tr>
                    </thead>
                    <tbody>
                        {renderWeather()}
                    </tbody>
                </Table>
        
    
    return (
        <div>
            { content }
        </div>
        )
}